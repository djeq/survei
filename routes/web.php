<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\InstrumensController;
use App\Http\Controllers\SurveisController;
use App\Http\Controllers\DivisisController;
use App\Http\Controllers\InstrumenDivisisController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get("/",[PagesController::class,'index']);
//User
// Route::get("/users",[UsersController::class,'index']);
// Route::get("/users/create",[UsersController::class,'create']);
// Route::post("/users",[UsersController::class,'store']);
// Route::get("/users/{user}",[UsersController::class,'show']);
// Route::delete('/users/{user}',[UsersController::class,'destroy']);
// Route::get('/users/{user}/edit',[UsersController::class,'edit']);
// Route::patch('/users/{user}',[UsersController::class,'update']);

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::middleware(['middleware','auth'])->group(function () {
    Route::resource('/users', UsersController::class);
    Route::resource('/divisis', DivisisController::class);
    Route::resource('/instrumens', InstrumensController::class);
    Route::resource('/instdivs', InstrumenDivisisController::class);
    Route::resource('/surveis', SurveisController::class);


});