@extends('layout.main')

@section('title', 'Detail Survei Form')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-7">
        <h1 class="mt-3">Detail Survey</h1>

        <div class="card">
    <div class="card-body">
    <h5 class="card-title">{{ $hasil['nama'] }}</h5>
    <p class="card-text">{{ $hasil['pertanyaan']}}</p>
    <p class="card-text">{{ $hasil['jawaban']}}</p>
    <a href="{{ $survei->id }}/edit" class="btn btn-primary">EDIT</a>
    <form action="{{ $survei->id }}" method="post" class="d-inline">
        @method('delete')
        @csrf
    <button type="delete" class="btn btn-danger">DELETE</button>
    <a href="/surveis" class="btn btn-success">BACK</a>
    </form>
    </div>
    </div>
        
            </div>
        </div>
    </div>
@endsection 