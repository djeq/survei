@extends('layout.main')

@section('title', 'Surveis Form')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-7">
        <h1 class="mt-3">Daftar Survey</h1>
    <a href="/surveis/create" class="btn btn-primary">ADD NEW SURVEY DATA</a>
    
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
        <ul class="list-group">
    @foreach ( $surveis as $survei )
    <li class="list-group-item d-flex justify-content-between align-items-center">
    {{ $survei->user->nama }}
    <a href="/surveis/{{ $survei->id }}" class="btn btn-primary">DETAIL</a>
    </li>
    @endforeach
        </ul>
            </div>
        </div>
    </div>
@endsection 
