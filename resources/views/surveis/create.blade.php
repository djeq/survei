@extends('layout.main')

@section('title', 'Add New Survey Data Form')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-7">
        <h1 class="mt-3">Add New Survey</h1>
        <form method="post" action="/surveis">
            @csrf

            <div class="mb-3">
            <label class="form-label">User</label>
            <select name="user_id" class="form-control  @error('user_id') is-invalid @enderror">
                <option value="">-Pilih User-</option>
                @foreach ($users as $user)
                <option value="{{$user->id}}" {{old('user_id') == $user->id ? 'selected' : null}}>{{$user->nama}}</option>
                @endforeach     
            </select>
            @error('user_id')<div class="invalid-feedback">{{$message}}</div>@enderror
            </div>

            <div class="mb-3">
            <label class="form-label">Pertanyaan</label>
            <select name="instrumen_id" class="form-control  @error('instrumen_id') is-invalid @enderror">
                <option value="">-Pilih Pertanyaan-</option>
                @foreach ($instrumens as $instrumen)
                <option value="{{$instrumen->id}}" {{old('instrumen_id') == $instrumen->id ? 'selected' : null}}>{{$instrumen->pertanyaan}}</option>
                @endforeach     
            </select>
            @error('instrumen_id')<div class="invalid-feedback">{{$message}}</div>@enderror
            </div>

            <div class="btn-group" role="group" aria-label="Basic radio toggle button group @error('jawaban') is-invalid @enderror">

                <input type="radio" class="btn-check" name="jawaban" id="jawaban1" autocomplete="off" value='1' checked>
                <label class="btn btn-outline-primary" for="jawaban1" >Sangat Baik</label>

                <input type="radio" class="btn-check" name="jawaban" id="jawaban2" autocomplete="off" value='2'>
                <label class="btn btn-outline-primary" for="jawaban2" >Baik</label>

                <input type="radio" class="btn-check" name="jawaban" id="jawaban3" autocomplete="off" value='3'>
                <label class="btn btn-outline-primary" for="jawaban3" >Kurang Baik</label>

                <input type="radio" class="btn-check" name="jawaban" id="jawaban4" autocomplete="off" value='4'>
                <label class="btn btn-outline-primary" for="jawaban4" >Tidak Baik</label>

                @error('jawaban')<div class="invalid-feedback">{{$message}}</div>@enderror
            </div>
            <button type="submit" class="btn btn-success">SEND</button>
            <a href="/surveis" class="btn btn-danger">CANCEL</a>
        </form>
            </div>
        </div>
    </div>
@endsection 