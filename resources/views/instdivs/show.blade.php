@extends('layout.main')

@section('title', 'Detail Instrumen Form')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-7">
        <h1 class="mt-3">Detail Devision Instrumen</h1>

        <div class="card">
    <div class="card-body">
    <h5 class="card-title">{{ $instdiv->instrumen->nama }}</h5>
    <p class="card-text">{{ $instdiv->divisi->nama}}</p>
    <a href="{{ $instdiv->id }}/edit" class="btn btn-primary">EDIT</a>
    <form action="{{ $instdiv->id }}" method="post" class="d-inline">
        @method('delete')
        @csrf
    <button type="delete" class="btn btn-danger">DELETE</button>
    <a href="/instdivs" class="btn btn-success">BACK</a>
    </form>
    </div>
    </div>
        
            </div>
        </div>
    </div>
@endsection 