@extends('layout.main')

@section('title', 'Add New Instrumen Division Data Form')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-7">
        <h1 class="mt-3">Add New Instrumen Division</h1>
        <form method="post" action="/instdivs">
            @csrf


            <div class="mb-3">
            <label class="form-label">Nama</label>
            <select name="instrumen_id" class="form-control  @error('instrumen_id') is-invalid @enderror">
                <option value="">-Pilih-</option>
                @foreach ($instrumens as $instrumen)
                <option value="{{$instrumen->id}}" {{old('instrumen_id') == $instrumen->id ? 'selected' : null}}>{{$instrumen->nama}}</option>
                @endforeach     
            </select>
            @error('instrumen_id')<div class="invalid-feedback">{{$message}}</div>@enderror
            </div>

            <div class="mb-3">
            <label class="form-label">Divisi</label>
            <select name="divisi_id" class="form-control  @error('divisi_id') is-invalid @enderror">
                <option value="">-Pilih Divisi-</option>
                @foreach ($divisi as $div)
                <option value="{{$div->id}}" {{old('divisi_id') == $div->id ? 'selected' : null}}>{{$div->nama}}</option>
                @endforeach     
            </select>
            @error('divisi_id')<div class="invalid-feedback">{{$message}}</div>@enderror
            </div>

            <button type="submit" class="btn btn-success">SEND</button>
            <a href="/instdivs" class="btn btn-danger">CANCEL</a>
        </form>
            </div>
        </div>
    </div>
@endsection 