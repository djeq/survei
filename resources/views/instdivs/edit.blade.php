@extends('layout.main')

@section('title', 'Edit Data Instrumen Form')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-7">
        <h1 class="mt-3">Edit Devision Data</h1>
        <form method="post" action="/instdivs/{{ $instdiv->id }}">
            @method('patch')
            @csrf

            <div class="mb-3">
            <label class="form-label">Nama</label>
            <select name="instrumen_id" class="form-control  @error('instrumen_id') is-invalid @enderror">
                <option value=""></option>
                @foreach ($instrumens as $instrumen)
                <option value="{{$instrumen->id}}" {{($instdiv->instrumen_id) == $instrumen->id ? 'selected' : null}}>{{$instrumen->nama}}</option>
                @endforeach     
            </select>
            @error('instrumen_id')<div class="invalid-feedback">{{$message}}</div>@enderror
            </div>

            <div class="mb-3">
            <label class="form-label">Divisi</label>
            <select name="divisi_id" class="form-control  @error('divisi_id') is-invalid @enderror">
                <option value=""></option>
                @foreach ($divisi as $div)
                <option value="{{$div->id}}" {{($instdiv->divisi_id) == $div->id ? 'selected' : null}}>{{$div->nama}}</option>
                @endforeach     
            </select>
            @error('divisi_id')<div class="invalid-feedback">{{$message}}</div>@enderror
            </div>
            <button type="submit" class="btn btn-success" >CONFIRM</button>
            <a href="/instdivs" class="btn btn-danger">CANCEL</a>
        </form>
            </div>
        </div>
    </div>
@endsection 