@extends('layout.main')

@section('title', 'Division Instrumen Form')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-7">
        <h1 class="mt-3">Daftar Division Instrumen</h1>
    <a href="/instdivs/create" class="btn btn-primary">ADD NEW  DATA</a>
    
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
        <ul class="list-group">
    @foreach ( $instdivs as $instdiv )
    <li class="list-group-item d-flex justify-content-between align-items-center">
    {{ $instdiv->instrumen->nama }}
    <a href="/instdivs/{{ $instdiv->id }}" class="btn btn-primary">DETAIL</a>
    </li>
    @endforeach
        </ul>
            </div>
        </div>
    </div>
@endsection 
