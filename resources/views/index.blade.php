@extends('layout/main')

@section('title', 'Laman User')

@section('container')
<div class="p-3 mb-2 bg-info text-dark">
    <div class="container">
        <div class="row">
            <div class="col-10">
        <h1 class="mt-3">Hello, Stalker!!</h1>
    <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center sm:pt-0">
            @if (Route::has('login'))
                <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                    @auth
                        <a href="{{ url('/dashboard') }}" class="btn btn-success">Dashboard</a>
                    @else
                        <a href="{{ route('login') }}" class="btn btn-danger">Log in</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="btn btn-warning">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
    </div>
            </div>
        </div>
    </div>
</div>
@endsection 
