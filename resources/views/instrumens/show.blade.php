@extends('layout.main')

@section('title', 'Detail Instrumen Form')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-7">
        <h1 class="mt-3">Detail Pengisi Instrumen</h1>

        <div class="card">
    <div class="card-body">
    <h5 class="card-title">{{ $instrumen->nama }}</h5>
    <p class="card-text">{{ $instrumen->pertanyaan}}</p>
    <a href="{{ $instrumen->id }}/edit" class="btn btn-primary">EDIT</a>
    <form action="{{ $instrumen->id }}" method="post" class="d-inline">
        @method('delete')
        @csrf
    <button type="delete" class="btn btn-danger">DELETE</button>
    <a href="/instrumens" class="btn btn-success">BACK</a>
    </form>
    </div>
    </div>
        
            </div>
        </div>
    </div>
@endsection 