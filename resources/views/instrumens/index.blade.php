@extends('layout.main')

@section('title', 'Instrumens Form')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-7">
        <h1 class="mt-3">Daftar Instrumen</h1>
    <a href="/instrumens/create" class="btn btn-primary">ADD NEW INSTRUMEN DATA</a>
    
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
        <ul class="list-group">
    @foreach ( $instrumens as $instrumen )
    <li class="list-group-item d-flex justify-content-between align-items-center">
    {{ $instrumen->nama }}
    <a href="/instrumens/{{ $instrumen->id }}" class="btn btn-primary">DETAIL</a>
    </li>
    @endforeach
        </ul>
            </div>
        </div>
    </div>
@endsection 