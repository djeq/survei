@extends('layout.main')

@section('title', 'Edit Data Instrumen Form')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-7">
        <h1 class="mt-3">Edit Instrumen Data</h1>
        <form method="post" action="/instrumens/{{ $instrumen->id }}">
            @method('patch')
            @csrf

            <div class="mb-3">
            <label for="nama" class="form-label">Nama</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" placeholder="Masukan nama" name="nama" value="{{$instrumen->nama}}">
            @error('nama')<div class="invalid-feedback">{{$message}}</div>@enderror
            </div>

            <div class="mb-3">
            <label for="pertanyaan" class="form-label">Isi Pertanyaan</label>
            <input type="text" class="form-control @error('pertanyaan') is-invalid @enderror" id="pertanyaan" placeholder="Masukan pertanyaan" name="pertanyaan" value="{{$instrumen->pertanyaan}}">
            @error('pertanyaan')<div class="invalid-feedback">{{$message}}</div>@enderror
            </div>
            <button type="submit" class="btn btn-success" >CONFIRM</button>
            <a href="/instrumens" class="btn btn-danger">CANCEL</a>
        </form>
            </div>
        </div>
    </div>
@endsection 