@extends('layout.main')

@section('title', 'Add New Instrumen Form')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-7">
        <h1 class="mt-3">Add Data Instrumen</h1>
        <form method="post" action="/instrumens">
            @csrf
            <div class="mb-3">
            <label for="nama" class="form-label">Nama</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" placeholder="Masukan Nama" name="nama" value="{{old('nama')}}">
            @error('nama')<div class="invalid-feedback">{{$message}}</div>@enderror
            </div>

            <div class="mb-3">
            <label for="pertanyaan" class="form-label">Isi Pertanyaan</label>
            <input type="text" class="form-control  @error('pertanyaan') is-invalid @enderror" id="pertanyaan" placeholder="Masukan pertanyaan" name="pertanyaan" value="{{old('pertanyaan')}}">
            @error('pertanyaan')<div class="invalid-feedback">{{$message}}</div>@enderror
            </div>

            <button type="submit" class="btn btn-success">SEND</button>
            <a href="/instrumens" class="btn btn-danger">CANCEL</a>
        </form>
            </div>
        </div>
    </div>
@endsection 