@extends('layout.main')

@section('title', 'Divisi Form')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-7">
        <h1 class="mt-3">Daftar Divisi</h1>
    <a href="/divisis/create" class="btn btn-primary">ADD NEW DIVISION DATA</a>
    
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
        <ul class="list-group">
    @foreach ( $divisis as $divisi )
    <li class="list-group-item d-flex justify-content-between align-items-center">
    {{ $divisi->nama }}
    <a href="/divisis/{{ $divisi->id }}" class="btn btn-primary">DETAIL</a>
    </li>
    @endforeach
        </ul>
            </div>
        </div>
    </div>
@endsection 