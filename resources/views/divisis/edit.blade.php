@extends('layout.main')

@section('title', 'Edit Data Divisi Form')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-7">
        <h1 class="mt-3">Edit Divisi Data/h1>
        <form method="post" action="/divisis/{{ $divisi->id }}">
            @method('patch')
            @csrf

            <div class="mb-3">
            <label for="nama" class="form-label">Nama Divisi</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" placeholder="Masukan nama" name="nama" value="{{$divisi->nama}}">
            @error('nama')<div class="invalid-feedback">{{$message}}</div>@enderror
            </div>

            <button type="submit" class="btn btn-success" >CONFIRM</button>
            <a href="/divisis" class="btn btn-danger">CANCEL</a>
        </form>
            </div>
        </div>
    </div>
@endsection 