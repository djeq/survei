@extends('layout.main')

@section('title', 'Add New Division Form')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-7">
        <h1 class="mt-3">Add Data Division</h1>
        <form method="post" action="/divisis">
            @csrf
            <div class="mb-3">
            <label for="nama" class="form-label">Nama Divisi</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" placeholder="Masukan Nama" name="nama" value="{{old('nama')}}">
            @error('nama')<div class="invalid-feedback">{{$message}}</div>@enderror
            </div>

            <button type="submit" class="btn btn-success">SEND</button>
            <a href="/divisis" class="btn btn-danger">CANCEL</a>
        </form>
            </div>
        </div>
    </div>
@endsection 