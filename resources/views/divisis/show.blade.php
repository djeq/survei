@extends('layout.main')

@section('title', 'Detail Instrumen Form')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-7">
        <h1 class="mt-3">Detail Division</h1>

        <div class="card">
    <div class="card-body">
    <h5 class="card-title">{{ $divisi->nama }}</h5>
    <a href="{{ $divisi->id }}/edit" class="btn btn-primary">EDIT</a>
    <form action="{{ $divisi->id }}" method="post" class="d-inline">
        @method('delete')
        @csrf
    <button type="delete" class="btn btn-danger">DELETE</button>
    <a href="/divisis" class="btn btn-success">BACK</a>
    </form>
    </div>
    </div>
        
            </div>
        </div>
    </div>
@endsection 