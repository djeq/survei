<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InstDiv extends Model
{
    use HasFactory;
    protected $table='instrumen_divisi';
    protected $id='id';
    protected $fillable=[
        'instrumen_id','divisi_id'
    ];
    public function Instrumen()
    {
        return $this->belongsTo(Instrumen::class, 'instrumen_id');
    }
    public function divisi()
    {
        return $this->belongsTo(divisi::class, 'divisi_id');
    }
}
