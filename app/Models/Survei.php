<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Survei extends Model
{
    use HasFactory;
    protected $table='survei';
    protected $id='id';
    protected $fillable=[
        'user_id',
        'instrumen_id',
        'jawaban'
    ];
    public function User()
    {
        return $this->belongsTo(Pemakai::class, 'user_id');
    }
    public function Instrumen()
    {
        return $this->belongsTo(Instrumen::class, 'instrumen_id');
    }
}
