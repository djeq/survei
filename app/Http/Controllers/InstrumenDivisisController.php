<?php

namespace App\Http\Controllers;

use App\Models\InstDiv;
use App\Models\divisi;
use App\Models\instrumen;
use Illuminate\Http\Request;

class InstrumenDivisisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $instdivs = InstDiv::all();
        return view('instdivs.index', compact('instdivs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $instrumens = Instrumen::all();
        $divisi = divisi::all();
        return view('instdivs.create', compact('instrumens','divisi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'instrumen_id'=>'required',
            'divisi_id'=>'required'
        ]);

        InstDiv::create($request->all());

        return redirect('/instdivs')->with('status','Berhasil Menambahkan Data ');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InstDiv  $instDiv
     * @return \Illuminate\Http\Response
     */
    public function show(InstDiv $instdiv)
    {
        return view('instdivs.show', compact('instdiv'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InstDiv  $instDiv
     * @return \Illuminate\Http\Response
     */
    public function edit(InstDiv $instdiv)
    {
        $instrumens = Instrumen::all();
        $divisi = divisi::all();
        return view('instdivs.edit', compact('instdiv','instrumens','divisi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InstDiv  $instDiv
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InstDiv $instdiv)
    {
        $request->validate([
            'instrumen_id'=>'required',
            'divisi_id'=>'required'
        ]);

        InstDiv::where('id', $instdiv->id)
                ->update([
                    'instrumen_id' =>$request->instrumen_id,
                    'divisi_id'=>$request->divisi_id
                ]);

                return redirect('/instdivs')->with('status','Berhasil Mengubah Data ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InstDiv  $instDiv
     * @return \Illuminate\Http\Response
     */
    public function destroy(InstDiv $instdiv)
    {
        InstDiv::destroy($instdiv->id);
        return redirect('/instdivs')->with('status','Berhasil Di Hapus');
    }
}
