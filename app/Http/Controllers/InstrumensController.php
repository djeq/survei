<?php

namespace App\Http\Controllers;

use App\Models\Instrumen;
use Illuminate\Http\Request;

class InstrumensController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $instrumens = Instrumen::all();
        return view('instrumens.index', compact('instrumens'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('instrumens.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama'=>'required',
            'pertanyaan'=>'required'
        ]);

        Instrumen::create($request->all());

        return redirect('/instrumens')->with('status','Berhasil Menambahkan Instrumen');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Instrumen  $instrumen
     * @return \Illuminate\Http\Response
     */
    public function show(Instrumen $instrumen)
    {
        return view('instrumens.show', compact('instrumen'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Instrumen  $instrumen
     * @return \Illuminate\Http\Response
     */
    public function edit(Instrumen $instrumen)
    {
        return view('instrumens.edit', compact('instrumen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Instrumen  $instrumen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Instrumen $instrumen)
    {
        $request->validate([
            'nama'=>'required',
            'pertanyaan'=>'required'
        ]);

        Instrumen::where('id', $instrumen->id)
                ->update([
                    'nama' =>$request->nama,
                    'pertanyaan'=>$request->pertanyaan
                ]);

                return redirect('/instrumens')->with('status','Berhasil Mengubah Data Instrumen');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Instrumen  $instrumen
     * @return \Illuminate\Http\Response
     */
    public function destroy(Instrumen $instrumen)
    {
        Instrumen::destroy($instrumen->id);
        return redirect('/instrumens')->with('status','Berhasil Di Hapus');
    }
}
