<?php

namespace App\Http\Controllers;

use App\Models\Pemakai;
use App\Models\divisi;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Pemakai::all();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $divisi = divisi::all();
        return view('users.create', compact('divisi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'nama'=>'required',
            'alamat'=>'required',
            'tahun_masuk'=>'required',
            'divisi_id'=>'required'
        ]);

        Pemakai::create($request->all());

        return redirect('/users')->with('status','Berhasil Menambahkan Data User');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pemakai  $user
     * @return \Illuminate\Http\Response
     */
    public function show(Pemakai $user)
    {
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pemakai  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Pemakai $user)
    {
        $divisi = divisi::all();
        return view('users.edit', compact('user','divisi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pemakai  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pemakai $user)
    {
        $request->validate([
            'nama'=>'required',
            'alamat'=>'required',
            'tahun_masuk'=>'required',
            'divisi_id'=>'required'
        ]);

        Pemakai::where('id', $user->id)
                ->update([
                    'nama' =>$request->nama,
                    'alamat'=>$request->alamat,
                    'tahun_masuk'=>$request->tahun_masuk,
                    'divisi_id'=>$request->divisi_id
                ]);

                return redirect('/users')->with('status','Berhasil Mengubah Data User');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pemakai  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pemakai $user)
    {
        Pemakai::destroy($user->id);
        return redirect('/users')->with('status','Berhasil Di Hapus');
    }
}
