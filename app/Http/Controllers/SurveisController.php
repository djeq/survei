<?php

namespace App\Http\Controllers;

use App\Models\Pemakai;
use App\Models\Instrumen;
use App\Models\Survei;
use Illuminate\Http\Request;

class SurveisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $surveis = Survei::all();
        return view('surveis.index', compact('surveis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = Pemakai::all();
        $instrumens = Instrumen::all();
        return view('surveis.create', compact('users','instrumens'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id'=>'required',
            'instrumen_id'=>'required',
            'jawaban'=>'required'
        ]);

        Survei::create($request->all());

        return redirect('/surveis')->with('status','Berhasil Menambahkan Data Survey');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Survei  $survei
     * @return \Illuminate\Http\Response
     */
    public function show(Survei $survei)
    {
        $data = array(
            "1"=> "Sangat Baik",
            "2"=>"Baik",
            "3"=>"Kurang Baik",
            "4"=>"Tidak Baik"
            );

        $hasil = array(
            'nama'=> $survei->user->nama,
            'pertanyaan' => $survei->instrumen->pertanyaan,
            'jawaban' => $data[$survei->jawaban]

        );
        return view('surveis.show', compact('survei','hasil'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Survei  $survei
     * @return \Illuminate\Http\Response
     */
    public function edit(Survei $survei)
    {
        $users = Pemakai::all();
        $instrumens = Instrumen::all();
        return view('surveis.edit', compact('survei','users','instrumens'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Survei  $survei
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Survei $survei)
    {
        $request->validate([
            'user_id'=>'required',
            'instrumen_id'=>'required',
            'jawaban'=>'required'
        ]);

        Survei::where('id', $survei->id)
                ->update([
                    'user_id' =>$request->user_id,
                    'instrumen_id'=>$request->instrumen_id,
                    'jawaban'=>$request->jawaban
                ]);

                return redirect('/surveis')->with('status','Berhasil Mengubah Data Survey');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Survei  $survei
     * @return \Illuminate\Http\Response
     */
    public function destroy(Survei $survei)
    {
        Survei::destroy($survei->id);
        return redirect('/surveis')->with('status','Berhasil Di Hapus');
    }
}
