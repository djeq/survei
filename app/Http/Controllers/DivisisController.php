<?php

namespace App\Http\Controllers;

use App\Models\divisi;
use Illuminate\Http\Request;

class DivisisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $divisis = divisi::all();
        return view('divisis.index', compact('divisis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('divisis.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama'=>'required',
        ]);

        divisi::create($request->all());

        return redirect('/divisis')->with('status','Berhasil Menambahkan Divisi Baru');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\divisi  $divisi
     * @return \Illuminate\Http\Response
     */
    public function show(divisi $divisi)
    {
        return view('divisis.show', compact('divisi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\divisi  $divisi
     * @return \Illuminate\Http\Response
     */
    public function edit(divisi $divisi)
    {
        return view('divisis.edit', compact('divisi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\divisi  $divisi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, divisi $divisi)
    {
        $request->validate([
            'nama'=>'required'
        ]);

        divisi::where('id', $divisi->id)
                ->update([
                    'nama' =>$request->nama
                ]);

                return redirect('/divisis')->with('status','Berhasil Mengubah Data Divisi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\divisi  $divisi
     * @return \Illuminate\Http\Response
     */
    public function destroy(divisi $divisi)
    {
        divisi::destroy($divisi->id);
        return redirect('/divisis')->with('status','Berhasil Di Hapus');
    }
}
